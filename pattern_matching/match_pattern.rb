def match_pattern(string, ptrn, assignedValues = {})
  c = ptrn[0]

  # String and pattern were exhausted
  if ptrn.length == 0 && string.length == 0
    return true
  end

  # Pattern or string reached its end
  if ptrn.length == 0 || string.length == 0
    return false
  end

  # See if value of key of char in pattern is a prefix of the string
  if assignedValues.key?(c)
    if string.start_with?(assignedValues[c])
      rest = string.delete_prefix(assignedValues[c])
      return match_pattern(rest, ptrn[1..-1], assignedValues)
    end
    return false
  else

    # Loop through possible assignments and recurse on each looking for
    # one that will work
    string.each_char.with_index do |char, index|
      prefix = string[0..index]
      remaining = string[index+1..-1]

      # Check to see if prefix was already assigned
      if !assignedValues.invert.key?(prefix)
        assignedValues[c] = prefix
      end

      if match_pattern(remaining, ptrn[1..-1], assignedValues)
        return true
      end
    end

    # Remove failed assignment
    assignedValues.delete(c)
    return false
  end
end

# Example
str = "redblueredblue"
pattern = "abab"

# str = "asdasdasdasd"
# pattern = "aaaa"

# str = "xyzabcxzyabc"
# pattern = "aabb"

match_pattern(str, pattern)
